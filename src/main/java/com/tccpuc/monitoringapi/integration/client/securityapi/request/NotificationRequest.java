package com.tccpuc.monitoringapi.integration.client.securityapi.request;

import com.tccpuc.monitoringapi.core.enums.MonitoringEventTypeEnum;
import lombok.Data;

@Data
public class NotificationRequest {

  private MonitoringEventTypeEnum type;
  private String sensorLocalization;
  private String monitoringEventDescription;

}
