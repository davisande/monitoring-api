package com.tccpuc.monitoringapi.integration.client.securityapi;

import com.tccpuc.monitoringapi.integration.client.securityapi.request.NotificationRequest;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(name = "security", url = "${integration.security-api.url}")
public interface SecurityClient {

  @RequestMapping(method = RequestMethod.POST, value = "${integration.security-api.notification-resource}")
  void sendNotification(@RequestBody NotificationRequest notificationRequest);

}
