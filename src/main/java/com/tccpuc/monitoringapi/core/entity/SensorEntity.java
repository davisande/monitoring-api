package com.tccpuc.monitoringapi.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "sensor")
public class SensorEntity {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Column
  private String code;

  @Column
  private String localization;

}
