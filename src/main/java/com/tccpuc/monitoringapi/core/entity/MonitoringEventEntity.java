package com.tccpuc.monitoringapi.core.entity;

import static javax.persistence.GenerationType.IDENTITY;

import com.tccpuc.monitoringapi.core.enums.MonitoringEventOriginEnum;
import com.tccpuc.monitoringapi.core.enums.MonitoringEventTypeEnum;
import java.time.Instant;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Data
@Entity
@Table(name = "monitoring_event")
@EntityListeners(AuditingEntityListener.class)
public class MonitoringEventEntity {

  @Id
  @GeneratedValue(strategy = IDENTITY)
  private Integer id;

  @Enumerated(EnumType.STRING)
  private MonitoringEventOriginEnum origin;

  @Enumerated(EnumType.STRING)
  private MonitoringEventTypeEnum type;

  @Column
  private String description;

  @CreatedDate
  @Column(name = "creation_date")
  private Instant creationDate;

  @OneToOne
  private SensorEntity sensor;

}
