package com.tccpuc.monitoringapi.core.advice;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Error {

  private String message;

}
