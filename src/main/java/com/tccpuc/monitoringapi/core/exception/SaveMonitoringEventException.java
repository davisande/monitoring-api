package com.tccpuc.monitoringapi.core.exception;

public class SaveMonitoringEventException extends RuntimeException {

  private static final String DEFAULT_MESSAGE = "Error saving monitoring event";

  public SaveMonitoringEventException() {
    super(DEFAULT_MESSAGE);
  }

}
