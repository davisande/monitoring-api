package com.tccpuc.monitoringapi.core.exception;

public class SaveDamEventException extends RuntimeException {

  private static final String DEFAULT_MESSAGE = "Error saving and sending dam event";

  public SaveDamEventException() {
    super(DEFAULT_MESSAGE);
  }

}
