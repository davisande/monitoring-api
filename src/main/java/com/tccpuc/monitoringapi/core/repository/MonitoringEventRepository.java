package com.tccpuc.monitoringapi.core.repository;

import com.tccpuc.monitoringapi.core.entity.MonitoringEventEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MonitoringEventRepository extends JpaRepository<MonitoringEventEntity, Integer> {

}
