package com.tccpuc.monitoringapi.core.repository;

import com.tccpuc.monitoringapi.core.entity.SensorEntity;
import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SensorRepository extends JpaRepository<SensorEntity, Integer> {

  SensorEntity findByCode(String code);

}
