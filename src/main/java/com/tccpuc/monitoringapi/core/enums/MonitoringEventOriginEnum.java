package com.tccpuc.monitoringapi.core.enums;

import static lombok.AccessLevel.PRIVATE;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor(access = PRIVATE)
public enum MonitoringEventOriginEnum {
  S("SENSOR"),
  A("APP");

  private String value;

}
