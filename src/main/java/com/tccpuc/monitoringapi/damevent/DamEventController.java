package com.tccpuc.monitoringapi.damevent;

import static java.util.Optional.ofNullable;

import com.tccpuc.monitoringapi.core.exception.SaveDamEventException;
import com.tccpuc.monitoringapi.damevent.mapper.RegisterDamEventRequestMapper;
import com.tccpuc.monitoringapi.damevent.dto.RegisterDamEventRequest;
import com.tccpuc.monitoringapi.damevent.service.DamEventService;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RequiredArgsConstructor
@RestController
public class DamEventController {

  private final RegisterDamEventRequestMapper registerDamEventRequestMapper;
  private final DamEventService damEventService;

  @PostMapping("/dam-events/register")
  public ResponseEntity registerDamEvent(
      @RequestBody @Valid final RegisterDamEventRequest registerDamEventRequest) {
    return ofNullable(registerDamEventRequest)
        .map(registerDamEventRequestMapper::toDomain)
        .map(damEventService::saveAndNotifyDamEvent)
        .map(de -> new ResponseEntity(HttpStatus.CREATED))
        .orElseThrow(SaveDamEventException::new);
  }
}
