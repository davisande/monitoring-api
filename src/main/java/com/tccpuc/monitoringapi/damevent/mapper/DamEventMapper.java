package com.tccpuc.monitoringapi.damevent.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.tccpuc.monitoringapi.core.entity.MonitoringEventEntity;
import com.tccpuc.monitoringapi.damevent.dto.DamEvent;
import com.tccpuc.monitoringapi.integration.client.securityapi.request.NotificationRequest;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface DamEventMapper {

  @Mapping(source = "sensor.code", target = "sensorCode")
  @Mapping(source = "sensor.localization", target = "sensorLocalization")
  DamEvent toDomain(MonitoringEventEntity monitoringEventEntity);

  @Mapping(source = "sensorCode", target = "sensor.code")
  @Mapping(source = "sensorLocalization", target = "sensor.localization")
  MonitoringEventEntity toEntity(DamEvent damEvent);

  @Mapping(source = "description", target = "monitoringEventDescription")
  NotificationRequest toNotificationRequest(DamEvent damEvent);

}
