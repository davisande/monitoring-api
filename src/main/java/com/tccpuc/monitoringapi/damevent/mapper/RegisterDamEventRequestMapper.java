package com.tccpuc.monitoringapi.damevent.mapper;

import static org.mapstruct.ReportingPolicy.IGNORE;

import com.tccpuc.monitoringapi.damevent.dto.DamEvent;
import com.tccpuc.monitoringapi.damevent.dto.RegisterDamEventRequest;
import org.mapstruct.Mapper;

@Mapper(unmappedTargetPolicy = IGNORE)
public interface RegisterDamEventRequestMapper {

  DamEvent toDomain(RegisterDamEventRequest registerDamEventRequest);

}
