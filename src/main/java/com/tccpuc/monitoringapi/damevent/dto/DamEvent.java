package com.tccpuc.monitoringapi.damevent.dto;

import com.tccpuc.monitoringapi.core.enums.MonitoringEventOriginEnum;
import com.tccpuc.monitoringapi.core.enums.MonitoringEventTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DamEvent {
  private MonitoringEventOriginEnum origin;
  private MonitoringEventTypeEnum type;
  private String description;
  private String sensorCode;
  private String sensorLocalization;

}
