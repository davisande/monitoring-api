package com.tccpuc.monitoringapi.damevent.dto;

import com.tccpuc.monitoringapi.core.enums.MonitoringEventOriginEnum;
import com.tccpuc.monitoringapi.core.enums.MonitoringEventTypeEnum;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotEmpty;
import lombok.Data;

@Data
public class RegisterDamEventRequest {

  @Enumerated(EnumType.STRING)
  private MonitoringEventOriginEnum origin;

  @Enumerated(EnumType.STRING)
  private MonitoringEventTypeEnum type;

  @NotEmpty(message = "Description is mandatory")
  private String description;

  @NotEmpty(message = "Sensor code is mandatory")
  private String sensorCode;

}
