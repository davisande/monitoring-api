package com.tccpuc.monitoringapi.damevent.service;

import static java.util.Optional.of;
import static java.util.Optional.ofNullable;

import com.tccpuc.monitoringapi.core.entity.MonitoringEventEntity;
import com.tccpuc.monitoringapi.core.entity.SensorEntity;
import com.tccpuc.monitoringapi.core.exception.SaveDamEventException;
import com.tccpuc.monitoringapi.core.exception.SaveMonitoringEventException;
import com.tccpuc.monitoringapi.core.repository.MonitoringEventRepository;
import com.tccpuc.monitoringapi.core.repository.SensorRepository;
import com.tccpuc.monitoringapi.damevent.dto.DamEvent;
import com.tccpuc.monitoringapi.damevent.mapper.DamEventMapper;
import com.tccpuc.monitoringapi.integration.client.securityapi.SecurityClient;
import javax.transaction.Transactional;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class DamEventService {

  private final SensorRepository sensorRepository;
  private final MonitoringEventRepository monitoringEventRepository;
  private final DamEventMapper damEventMapper;
  private final SecurityClient securityClient;

  @Transactional
  public DamEvent saveAndNotifyDamEvent(@NonNull final DamEvent damEvent) {
    return of(performSaving(damEvent))
        .map(this::sendNotification)
        .orElseThrow(SaveDamEventException::new);
  }

  private DamEvent performSaving(final DamEvent damEvent) {
    return ofNullable(damEvent.getSensorCode())
        .map(sensorRepository::findByCode)
        .map(se -> buildMonitoringEventEntity(se, damEvent))
        .map(monitoringEventRepository::save)
        .map(damEventMapper::toDomain)
        .orElseThrow(SaveMonitoringEventException::new);
  }

  private MonitoringEventEntity buildMonitoringEventEntity(@NonNull final SensorEntity sensorEntity,
      final DamEvent damEvent) {
    return of(damEvent)
        .map(damEventMapper::toEntity)
        .map(mee -> setSensorIdInMonitoringEventEntity(sensorEntity, mee))
        .orElse(null);
  }

  private MonitoringEventEntity setSensorIdInMonitoringEventEntity(final SensorEntity sensorEntity,
      @NonNull final MonitoringEventEntity monitoringEventEntity) {
    monitoringEventEntity.setSensor(sensorEntity);

    return monitoringEventEntity;
  }

  private DamEvent sendNotification(final DamEvent damEvent) {
    of(damEvent)
        .map(damEventMapper::toNotificationRequest)
        .ifPresent(securityClient::sendNotification);

    return damEvent;
  }

}
