CREATE TABLE sensor (
	id INTEGER NOT NULL AUTO_INCREMENT,
	code VARCHAR(30) NOT NULL,
	localization VARCHAR(200),
	CONSTRAINT pk_sensor PRIMARY KEY (id)
);

CREATE TABLE monitoring_event (
	id INTEGER NOT NULL AUTO_INCREMENT,
	sensor_id INTEGER NULL,
	origin CHAR(1) NOT NULL,
	type CHAR(1) NOT NULL,
	description VARCHAR(300) NOT NULL,
	creation_date TIMESTAMP NOT NULL,
	CONSTRAINT pk_monitoring_event PRIMARY KEY (id),
	CONSTRAINT fk_sensor_monitoring_event FOREIGN KEY (sensor_id) REFERENCES sensor(id)
);